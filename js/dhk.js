var dhk = function() {}
dhk.clock=null;
dhk.state=0;
dhk.ophalen = "2017/12/29 18:00:00";
dhk.proef   = "2017/12/29 19:00:00";
dhk.proefSt = "2017/12/29 19:30:00";
dhk.brengen = "2017/12/29 23:30:00";
dhk.initClock = function(elem) {
  dhk.state=0;
  dhk.clock = $(elem).FlipClock({
    countdown:true,
    language:'dutch'
  });
};
dhk.nextState = function() {
  dhk.state++;
  dhk.clock.stop();
    switch(dhk.state) {
      case 1:
        dhk.clock.setTime(dhk.delta(dhk.ophalen));
        dhk.clock.start();
        $(".dhk-message").text("Ophalen boekjes");
        break;
      case 2:
        dhk.clock.setTime(dhk.delta(dhk.proef));
        dhk.clock.start();
        $(".dhk-message").text("Start geheime proef");
        $(".dhk-inschrijven").text("");
        break;
      case 3:
        dhk.clock.setTime(dhk.delta(dhk.proefSt));
        dhk.clock.start();
        $(".dhk-message").text("De geheime proef is gestart");
        $(".dhk-inschrijven").text("");
        break;
      case 4:
        dhk.clock.setTime(dhk.delta(dhk.brengen));
        dhk.clock.start();
        $(".dhk-message").text("Tot inleveren boekjes");
        $(".dhk-proef").text("");
        break;
      case 5:
        $(".dhk-message").text("Bedankt voor jullie inzet");
        $(".dhk-dhk-inleveren").text("");
        $('.dhk-clock').hide();
        break;
    }
};
dhk.delta = function (datestring) {
  var target  = new Date(datestring);
  var now  = new Date();
  //return 3;
  return target.getTime()/1000 -now.getTime()/1000;
}